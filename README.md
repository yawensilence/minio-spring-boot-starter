# minIO spring boot starter 1.2.1

## 简介
1. 使用minIO单bucket存储获取删除对象（文件）
2. 也可以获取对象输入流、直接写入输出流
3. 支持多个bucket，也兼容v1.0.0的配置默认bucket

## 使用方法：
### 依赖
```xml
<dependency>
    <groupId>com.jvm123</groupId>
    <artifactId>minio-spring-boot-starter</artifactId>
    <version>1.2.1</version>
</dependency>
```

### 配置
```yml
file:
  store:
    minio:
      endpoint: http://192.168.80.106:9000
      bucket: test
      access-key: admin
      secret-key: admin123
      tmp-dir: ./tmp/
      tmp-clean: true
      tmp-first-clean-time: 2020-1-17 11:45:00
      tmp-clean-period: 12960000
      tmp-alive-duration: 12960000
```
如果不配置默认bucket，则需要在使用时指定。存储时指定的bucket如果不存在，则会自动创建。  
tmp-dir 为api中返回File类型的缓存目录，这个目录会按照FIFO规则定时清理。如果使用返回InputStream的api，则不会有缓存。

### 使用
兼容v1.0.0的所有用发，新增的示例如下：

```java
package com.jvm123.demo;

import com.jvm123.minio.service.FileStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author yawn http://jvm123.com
 * 2020/1/15 17:17
 */
@RestController
public class TestController {

    @Autowired
    FileStoreService fileStoreService;

    @GetMapping("test2")
    public void test2(HttpServletResponse response) throws IOException {
        String bucketName = "demo-bucket";

        // 创建bucket
        boolean created = fileStoreService.createBucket(bucketName);

        // 存储文件
        String saved = fileStoreService.save(bucketName, new File("C:\\asd.txt"), "a.txt");

        // 删除文件
        boolean deleted = fileStoreService.delete(bucketName, "a.txt");

        // 获取文件
        File file = fileStoreService.getFile(bucketName, "a.txt");

        // 获取输入流
        InputStream inputStream = fileStoreService.getStream(bucketName, "a.txt");

        // 下载
        response.addHeader("Content-Disposition","attachment;filename=a.txt");
        ServletOutputStream os = response.getOutputStream();
        fileStoreService.writeTo(bucketName, "a.txt", os);
    }
}
```

## change list

v1.0.0 常用功能的实现  
1. 实现使用minIO单bucket存储获取对象（文件）的功能
2. 实现获取对象输入流、写入输出流等功能

v1.1.0 常用功能的实现  
1. 支持多个bucket，也兼容v1.0.0的配置默认bucket
2. 可创建删除bucket
3. 删除文件

v1.2.1 
1. 架构调整，可自定义minio客户端和缓存清理

---
# minIO spring boot starter 1.1.0

## 简介
1. 使用minIO单bucket存储获取删除对象（文件）
2. 也可以获取对象输入流、直接写入输出流
3. 支持多个bucket，也兼容v1.0.0的配置默认bucket

## 使用方法：
### 依赖
```xml
<dependency>
    <groupId>com.jvm123</groupId>
    <artifactId>minio-spring-boot-starter</artifactId>
    <version>1.1.0</version>
</dependency>
```

### 配置
```yml
file:
  store:
    minio:
      endpoint: http://192.168.80.106:9000
      bucket: test
      access-key: admin
      secret-key: admin123
      tmp-dir: ./tmp/
      tmp-clean: true
      tmp-first-clean-time: 2020-1-17 11:45:00
      tmp-clean-period: 12960000
      tmp-alive-duration: 12960000
```
如果不配置默认bucket，则需要在使用时指定。存储时指定的bucket如果不存在，则会自动创建。  
tmp-dir 为api中返回File类型的缓存目录，这个目录会按照FIFO规则定时清理。如果使用返回InputStream的api，则不会有缓存。

### 使用
兼容v1.0.0的所有用发，新增的示例如下：

```java
package com.jvm123.demo;

import com.jvm123.minio.service.FileStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author yawn http://jvm123.com
 * 2020/1/15 17:17
 */
@RestController
public class TestController {

    @Autowired
    FileStoreService fileStoreService;

    @GetMapping("test2")
    public void test2(HttpServletResponse response) throws IOException {
        String bucketName = "demo-bucket";

        // 创建bucket
        boolean created = fileStoreService.createBucket(bucketName);

        // 存储文件
        String saved = fileStoreService.save(bucketName, new File("C:\\asd.txt"), "a.txt");

        // 删除文件
        boolean deleted = fileStoreService.delete(bucketName, "a.txt");

        // 获取文件
        File file = fileStoreService.getFile(bucketName, "a.txt");

        // 获取输入流
        InputStream inputStream = fileStoreService.getStream(bucketName, "a.txt");

        // 下载
        response.addHeader("Content-Disposition","attachment;filename=a.txt");
        ServletOutputStream os = response.getOutputStream();
        fileStoreService.writeTo(bucketName, "a.txt", os);
    }
}
```

## change list

v1.0.0 常用功能的实现  
1. 实现使用minIO单bucket存储获取对象（文件）的功能
2. 实现获取对象输入流、写入输出流等功能

v1.1.0 常用功能的实现  
1. 支持多个bucket，也兼容v1.0.0的配置默认bucket
2. 可创建删除bucket
3. 删除文件

---
# minIO spring boot starter 1.0.0

## 简介
1. 使用minIO单bucket存储获取对象（文件）
2. 也可以获取对象输入流、直接写入输出流

## 使用方法：
### 依赖
```xml
<dependency>
    <groupId>com.jvm123</groupId>
    <artifactId>minio-spring-boot-starter</artifactId>
    <version>1.0.0</version>
</dependency>
```

### 配置
```yml
file:
  store:
    minio:
      endpoint: http://petdy.cn:9000
      bucket: test-bucket
      access-key: admin
      secret-key: admin
      tmp-dir: ./tmp/
```

### 使用
```java
package com.jvm123.demo;

import com.jvm123.minio.service.FileStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author yawn http://jvm123.com
 * 2020/1/15 17:17
 */
@RestController
public class TestController {

    @Autowired
    FileStoreService fileStoreService;

    @GetMapping("get")
    public void get(HttpServletResponse response) throws IOException {
        // 存储文件
        fileStoreService.save(new File("C:\\asd.txt"), "a.txt");
        
        // 获取文件
        File file = fileStoreService.getFile("a.txt");

        // 获取输入流
        InputStream inputStream = fileStoreService.getStream("a.txt");

        // 下载
        response.addHeader("Content-Disposition","attachment;filename=a.txt");
        ServletOutputStream os = response.getOutputStream();
        fileStoreService.download("a.txt", os);
    }
}
```

## change list

v1.0.0 常用功能的实现  
1. 实现使用minIO单bucket存储获取对象（文件）的功能
2. 实现获取对象输入流、写入输出流等功能

