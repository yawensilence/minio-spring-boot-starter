package com.jvm123.minio.service.temp;

/**
 * 清理缓存
 * @author yawn http://jvm123.com
 * 2020/1/18 10:54
 */
public interface TempCleanService {

    void cleanTemp(String tmpDir, Long tmpAliveDuration);
}
