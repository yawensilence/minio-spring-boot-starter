package com.jvm123.minio.service.client;

import io.minio.MinioClient;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author yawn http://jvm123.com
 * 2020/1/18 11:08
 */
public class MinClient implements MinioClientProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(MinioClient.class);

    private MinioClient minioClient = null;

    @Override
    public MinioClient getClient(String endpoint, String accessKey, String secretKey) {
        try {
            if (minioClient == null) {
                minioClient = new MinioClient(endpoint, accessKey, secretKey);
            }
            return minioClient;
        } catch (InvalidEndpointException | InvalidPortException e) {
            LOGGER.error("error: {}", e.getMessage());
            return null;
        }
    }
}
