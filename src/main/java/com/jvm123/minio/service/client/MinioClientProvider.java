package com.jvm123.minio.service.client;

import io.minio.MinioClient;

/**
 * minIO 客户端 provider
 * @author yawn http://jvm123.com
 * 2020/1/18 10:36
 */
public interface MinioClientProvider {

    /**
     * 获取一个MinioClient
     * @param endpoint 端点
     * @param accessKey accessKey
     * @param secretKey secretKey
     * @return MinioClient
     */
    MinioClient getClient(String endpoint, String accessKey, String secretKey);


}
