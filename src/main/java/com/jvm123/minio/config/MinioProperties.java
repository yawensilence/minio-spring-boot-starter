package com.jvm123.minio.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author yawn http://jvm123.com
 * 2020/1/15 15:55
 */
@Configuration
@ConfigurationProperties(prefix = "file.store.minio")
public class MinioProperties {

    private static final Long DAY_MILLIS = 1000L * 60 * 60 * 24;

    private String endpoint;
    private String bucket;
    private String accessKey;
    private String secretKey;
    private String tmpDir = "./tmp/";
    private Boolean tmpClean = true;
    private Date tmpFirstCleanTime;
    private Long tmpCleanPeriod = DAY_MILLIS;
    private Long tmpAliveDuration = DAY_MILLIS;

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public Boolean getTmpClean() {
        return tmpClean;
    }

    public void setTmpClean(Boolean tmpClean) {
        this.tmpClean = tmpClean;
    }

    public String getTmpDir() {
        return tmpDir;
    }

    public void setTmpDir(String tmpDir) {
        this.tmpDir = tmpDir;
    }

    public Date getTmpFirstCleanTime() {
        return tmpFirstCleanTime;
    }

    public void setTmpFirstCleanTime(String tmpFirstCleanTime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = format.parse(tmpFirstCleanTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.tmpFirstCleanTime = date;
    }

    public Long getTmpCleanPeriod() {
        return tmpCleanPeriod;
    }

    public void setTmpCleanPeriod(Long tmpCleanPeriod) {
        this.tmpCleanPeriod = tmpCleanPeriod;
    }

    public Long getTmpAliveDuration() {
        return tmpAliveDuration;
    }

    public void setTmpAliveDuration(Long tmpAliveDuration) {
        this.tmpAliveDuration = tmpAliveDuration;
    }
}
