package com.jvm123.minio.config;

import com.jvm123.minio.service.MinioFileService;
import com.jvm123.minio.service.client.MinioClientProvider;
import com.jvm123.minio.service.client.MinClient;
import com.jvm123.minio.service.temp.DefaultTempCleanService;
import com.jvm123.minio.service.temp.TempCleanService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author yawn http://jvm123.com
 * 2020/1/15 15:53
 */
@Configuration
@EnableConfigurationProperties(MinioProperties.class)
public class MinioAutoConfiguration {

    @Bean
    MinioFileService MinioFileService() {
        return new MinioFileService();
    }

    @Bean
    @ConditionalOnMissingBean(TempCleanService.class)
    TempCleanService tempCleanService() {
        return new DefaultTempCleanService();
    }

    @Bean
    @ConditionalOnMissingBean(MinioClientProvider.class)
    MinioClientProvider minClient() {
        return new MinClient();
    }
}
